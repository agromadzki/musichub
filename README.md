# MusicHub

![Screenshot](https://gitlab.com/agromadzki/musichub/-/raw/master/Front%20End%20Server/css/images/Screenshot_from_2021-04-16_18-42-48.png)

A simple music discovery website/catalog.

### Prerequisites

These directions are written assuming the user is running Ubuntu Server 18.04.

### Requirements:

Ensure you have the following packages installed on your system.

```
sudo apt-get install php apache2 mysql-server rabbitmq-server php-bcmath php-mbstring composer php-curl php-gettext php-mysql
```

With composer installed, run the following command:

```
composer require php-amqplib/php-amqplib
```

To set up the management console in RabbitMQ make sure you run:

```
sudo rabbitmq-plugins enable rabbitmq_management
```

### Installing

Below is everything you need to do to get a working copy of this project running on your machine(s).

1. Clone this repository.

2. Modify `.ini` files in each folder to use your RabbitMQ Server's IP address.

3. Make a database called "website" and import .sql files.

4. Run each file that has "receiver" in its name using `php <receiver_name>.php`. 

5. Then, make an administrative user in RabbitMQ, enter localhost:15672 in the address bar and test the application. 


## License

This project is licensed under the MIT License.
